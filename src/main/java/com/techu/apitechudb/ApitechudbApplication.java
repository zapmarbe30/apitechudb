package com.techu.apitechudb;

import com.techu.apitechudb.Models.ProductModels;
import com.techu.apitechudb.Models.UserModel;
import com.techu.apitechudb.Models.PurchaseModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.HashMap;

@SpringBootApplication
public class ApitechudbApplication {

	public static ArrayList<ProductModels> productModels;
	public static ArrayList<UserModel> userModels;
	public static ArrayList<PurchaseModel> purchaseModels;

	public static void main(String[] args) {
            SpringApplication.run(ApitechudbApplication.class, args);

            ApitechudbApplication.productModels = ApitechudbApplication.getTestData();
			ApitechudbApplication.userModels = ApitechudbApplication.getTestUserData();
			ApitechudbApplication.purchaseModels = ApitechudbApplication.getTestDataPurchase();
        }

        private static ArrayList<ProductModels> getTestData() {

            ArrayList<ProductModels> ProductModels = new ArrayList<>();

            ProductModels.add(
                    new ProductModels(
                            "1",
                            "Producto 1",
                            10
                    )
            );
            ProductModels.add(
                    new ProductModels(
                            "2",
                            "Producto 2",
                            20
                    )
            );
            ProductModels.add(
                    new ProductModels(
                            "3",
                            "Producto 3",
                            30
                    )
            );
            return ProductModels;
        }
		private static ArrayList<UserModel> getTestUserData() {

		ArrayList<UserModel> userModels = new ArrayList<>();

		userModels.add(
				new UserModel(
						"1",
						"Usuario 1",
						10
				)
		);
		userModels.add(
				new UserModel(
						"2",
						"Usuario 2",
						20
				)
		);
		userModels.add(
				new UserModel(
						"3",
						"Usuario 3",
						30
				)
		);
		return userModels;
	}
/*		private static ArrayList<PurchaseModel> getTestPurchaseData() {

		ArrayList<PurchaseModel> purchaseModels = new ArrayList<>();
		HashMap compra1= new HashMap();
		compra1.put("Producto1", 10);

		HashMap compra2= new HashMap();
		compra2.put("Producto2", 20);

		HashMap compra3= new HashMap();
		compra3.put("Producto3", 30);

		purchaseModels.add(
				new PurchaseModel(
						"1",
						"Usuario 1",
						10,
						compra1
				)
		);
		purchaseModels.add(
				new PurchaseModel(
						"2",
						"Usuario 2",
						20,
						compra2
				)
		);
		purchaseModels.add(
				new PurchaseModel(
						"3",
						"Usuario 3",
						30,
						compra3
				)
		);
		return purchaseModels;
	}
*/
		private static ArrayList<PurchaseModel> getTestDataPurchase(){

		return new ArrayList<>();

/*		ArrayList<PurchaseModel> purchaseModels = new ArrayList<>();

		HashMap compra1 = new HashMap();
		compra1.put("Producto 1",10);
		compra1.put("Producto 2",20);

		HashMap compra2 = new HashMap();
		compra2.put("Producto 2",20);
		compra2.put("Producto 3",30);

		HashMap compra3 = new HashMap();
		compra3.put("Producto 3",30);
		compra3.put("Producto 4",40);

		purchaseModels.add(
				new PurchaseModel(
						"1",
						"1",
						30,
						compra1
				)
		);

		purchaseModels.add(
				new PurchaseModel(
						"2",
						"2",
						50,
						compra2
				)
		);

		purchaseModels.add(
				new PurchaseModel(
						"3",
						"3",
						70,
						compra3
				)
		);

		return purchaseModels;*/
}
}
