package com.techu.apitechudb.Services;

import com.techu.apitechudb.Models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
        @Autowired
        com.techu.apitechudb.Repositories.UserRepository UserRepository;

        public List<UserModel> findAll(){
            System.out.println("findAll en UserService");
            return  this.UserRepository.findAll();
        }

    public Optional<UserModel> findById(String id){
        System.out.println("findById en UserService");
        return  this.UserRepository.findById(id);
    }

    public Optional<UserModel> findByAge(int age){
        System.out.println("findByAge en UserService");
//        int age1 = Integer.parseInt(age);
        return  this.UserRepository.findByAge(age);
    }

    public  UserModel add(UserModel user){
        System.out.println("add en UserModel");
        return this.UserRepository.save(user);
    }

    public UserModel update(UserModel userModel){
        System.out.println("update en UserModel");
        return  this.UserRepository.update(userModel);
    }

    public boolean delete(String id){
        System.out.println("delete en UserService");
        boolean result = false;
        Optional<UserModel> userDelete = this.findById(id);

        if(userDelete.isPresent() == true){
            result =true;
            this.UserRepository.delete(userDelete.get());
        }

        return result;
    }
    public Optional<UserModel> getById(String id) {
        System.out.println("getById en userService");
        System.out.println("La id es " + id);

        return this.UserRepository.findById(id);
    }
}
