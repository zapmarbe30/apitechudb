package com.techu.apitechudb.Services;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.Models.ProductModels;
import com.techu.apitechudb.Repositories.ProduRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProduService {

    @Autowired
    ProduRepository ProduRepository;

    public List <ProductModels> findAll(){
        System.out.println("findAll en ProductService");

           return  this.ProduRepository.findAll();
//        return ApitechudbApplication.productModels;
    }
    public  ProductModels add(ProductModels product){
        System.out.println("add en ProductService");

        return this.ProduRepository.save(product);
    }

    public Optional<ProductModels> findById(String id){
        System.out.println("findById en ProductService");
        return  this.ProduRepository.findById(id);
    }

    public ProductModels update(ProductModels productModels){
        System.out.println("update en ProductService");

        return  this.ProduRepository.update(productModels);
    }
    public boolean delete(String id){
        System.out.println("delete en productService");
        boolean result = false;
        Optional<ProductModels> producDelete = this.findById(id);

        if(producDelete.isPresent() == true){
            result =true;
            this.ProduRepository.delete(producDelete.get());
        }

        return result;
    }

}
