package com.techu.apitechudb.Services;

import com.techu.apitechudb.Models.PurchaseModel;
import com.techu.apitechudb.Repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserService userService;

    @Autowired
    ProduService productService;

    public PurchaseServiceResponse add(PurchaseModel purchase){
        System.out.println("add en PurchaseService");

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchase(purchase);

        if (this.userService.getById(purchase.getUserId()).isPresent() == false){
            System.out.println("El usuario de la compra no se ha encontrado");

            result.setMsg("El usuario de la compra no se ha encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if (this.getById(purchase.getPurchaseId()).isPresent() == false){
            System.out.println("Ya hay una compra con esa id");

            result.setMsg("Ya hay una compra con esa id");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        float amount = 0;
        for (Map.Entry<String,Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {
            if (!this.productService.findById(purchaseItem.getKey()).isPresent()) {
                System.out.println("El producto con la id " +  purchaseItem.getKey() +
                        " no se encuentra en el sistema");
                result.setMsg("El producto con la id " +  purchaseItem.getKey() +
                        " no se encuentra en el sistema");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
                return result;
            }else{
                System.out.println("Añadiendo valor de " + purchaseItem.getValue() + " unidades del producto al total");

                amount += this.productService.findById(purchaseItem.getKey()).get().getPrice()
                        * purchaseItem.getValue();
            }

        }

        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.OK);

        return result;

    }

    public List<PurchaseModel> getPurchases(){
        System.out.println("getPurchases en PurchaseService");

        return this.purchaseRepository.findAll();
    }

    public Optional<PurchaseModel> findById(String id){
        System.out.println("findById en PurchaseService");

        return this.purchaseRepository.findById(id);
    }

    public Optional<PurchaseModel> getById(String id) {
        System.out.println("getById en PurchaseService");
        System.out.println("La id es " + id);

        return this.purchaseRepository.findById(id);
    }

}
