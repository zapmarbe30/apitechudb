package com.techu.apitechudb.Repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.Models.UserModel;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.Optional;

@Repository
public class UserRepository {
    public ArrayList<UserModel> findAll(){
        System.out.println("findAll en UserRepository");

        return ApitechudbApplication.userModels;
    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById en UserRepository");
        Optional<UserModel> resultId = Optional.empty();

        for (UserModel userInList : ApitechudbApplication.userModels){
            if (userInList.getId().equals(id)){
                System.out.println("Usuario encontrado por id");
                resultId = Optional.of(userInList);
            }
        }
        return resultId;
    }

    public Optional<UserModel> findByAge(int age){
        System.out.println("findById en UserRepository");
        Optional<UserModel> resultAge = Optional.empty();

        for (UserModel userInList : ApitechudbApplication.userModels){
            if (userInList.getAge() == age){
                System.out.println("Usuario encontrado por edad");
                resultAge = Optional.of(userInList);
            }
        }
        return resultAge;
    }
    public UserModel save(UserModel user){
        System.out.println("Save en UserRepository");
        ApitechudbApplication.userModels.add(user);

        return user;
    }

    public UserModel update(UserModel user){
        System.out.println("update en UserRepository");

        Optional<UserModel> userToUpdate = this.findById(user.getId());

        if (userToUpdate.isPresent() == true){
            System.out.println("Usuario encontrado");

            UserModel userFromList = userToUpdate.get();

            userFromList.setName(user.getName());
            userFromList.setAge(user.getAge());
        }
        return user;
    }
    public void delete (UserModel user){
        System.out.println("delete en userRepository");
        System.out.println("Borrado usuario");

        ApitechudbApplication.userModels.remove(user);
    }
}
