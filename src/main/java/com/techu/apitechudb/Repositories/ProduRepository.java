package com.techu.apitechudb.Repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.Models.ProductModels;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProduRepository {

    public List<ProductModels> findAll(){
        System.out.println("findAll en ProductRepository");
        return ApitechudbApplication.productModels;
    }
    public ProductModels save(ProductModels product){
        System.out.println("Save en ProductRepository");
        ApitechudbApplication.productModels.add(product);

        return product;
    }
    public Optional<ProductModels> findById(String id){
        System.out.println("findById en ProducRepositry");

        Optional<ProductModels> result = Optional.empty();

        for (ProductModels productInList : ApitechudbApplication.productModels){
            if (productInList.getId().equals(id)){
                System.out.println("Producto encontrado");
                result = Optional.of(productInList);
            }
        }
        return result;

    }
     public ProductModels update(ProductModels products){
        System.out.println("update en ProducRepository");

        Optional<ProductModels> producToUpdate = this.findById(products.getId());

        if (producToUpdate.isPresent() == true){
            System.out.println("Producto encontrado");

            ProductModels productFromList = producToUpdate.get();

            productFromList.setDesc(products.getDesc());
            productFromList.setPrice(products.getPrice());
        }
        return products;
    }
     public void delete (ProductModels products){
        System.out.println("delete en ProductRepository");
        System.out.println("Borrado producto");

        ApitechudbApplication.productModels.remove(products);
     }
}
