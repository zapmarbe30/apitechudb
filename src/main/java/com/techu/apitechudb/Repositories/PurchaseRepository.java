package com.techu.apitechudb.Repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.Models.PurchaseModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository {

    public PurchaseModel save(PurchaseModel purchase){
        System.out.println("save en UserPurchase");

        ApitechudbApplication.purchaseModels.add(purchase);

        return purchase;
    }

    public List<PurchaseModel> findAll(){
        System.out.println("findAll en PurchaseRepository");

        return ApitechudbApplication.purchaseModels;
    }

    public Optional<PurchaseModel> findById(String id){
        System.out.println("findById en PurchaseRepository");

        Optional <PurchaseModel> result = Optional.empty();

        for (PurchaseModel userInList : ApitechudbApplication.purchaseModels){
            if (userInList.getPurchaseId().equals(id)){
                System.out.println("Compra encontrado");
                result = Optional.of(userInList);
            }
        }
        return result;
    }
}