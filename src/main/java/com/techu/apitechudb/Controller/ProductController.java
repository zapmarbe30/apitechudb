package com.techu.apitechudb.Controller;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.Models.ProductModels;
import com.techu.apitechudb.Repositories.ProduRepository;
import com.techu.apitechudb.Services.ProduService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping ("/apitechu/v2")
public class ProductController {

    @Autowired
    ProduService productService;

    @GetMapping ("/products")
    public ResponseEntity<List<ProductModels>> getProducts(){
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productService.findAll(),
        HttpStatus.OK
        );
//        return this.productService.findAll();
//        return ApitechudbApplication.productModels;
    }
    @PostMapping("/products")
    public  ResponseEntity<ProductModels> addProduct(@RequestBody ProductModels product){
     System.out.println("addProduct");
     System.out.println("La id del producto a crear es " + product.getId());
     System.out.println("La descripcion del producto a crear es" + product.getDesc());
     System.out.println("El precio del producto a crear es" + product.getDesc());

     return new ResponseEntity<>(
             this.productService.add(product),
             HttpStatus.CREATED
     );
    }
    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("La id del producto es " +id);

        Optional<ProductModels> result = this.productService.findById(id);

        return new ResponseEntity<>(
                        result.isPresent() ? result.get() : "Producto no encontrado",
                        result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/products/{id}")
    public  ResponseEntity<ProductModels> updateProduct(@RequestBody ProductModels product,@PathVariable String id){
        System.out.println("La id del producto que se va a actualizar en parametro es " +id);
        System.out.println("La id del producto que se va a actualizar es " +product.getId());
        System.out.println("La descripcion del producto que se va a actualizar es " +product.getDesc());
        System.out.println("El precio del producto que se va a actualizar es " +product.getPrice());

        return new ResponseEntity<>(this.productService.update(product), HttpStatus.OK);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("La id es " + id);

        boolean deleteProduct = this.productService.delete(id);
        return  new ResponseEntity<>(
                deleteProduct ? "Producto borrado" : "Producto no encontrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
