package com.techu.apitechudb.Controller;

import com.techu.apitechudb.Models.PurchaseModel;
import com.techu.apitechudb.Services.PurchaseService;
import com.techu.apitechudb.Services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/apitechu/v4")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases() {
        System.out.println("getPurchases");

        return new ResponseEntity<>(
                this.purchaseService.getPurchases(),
                HttpStatus.OK
        );
    }

    @PostMapping("/purchases")
    public ResponseEntity<Object> addPurchase(@RequestBody PurchaseModel purchase){
        System.out.println("addPurchase");
        System.out.println("La id de la compra a añadir es " + purchase.getPurchaseId());
        System.out.println("La id del usuario de la compra a añadir es " + purchase.getUserId());
        System.out.println("Los elementos de la compra son " + purchase.getPurchaseItems());

        PurchaseServiceResponse purchaseServiceResponse = this.purchaseService.add(purchase);

        return new ResponseEntity<>(
                purchaseServiceResponse.getPurchase(),
                purchaseServiceResponse.getResponseHttpStatusCode()
        );
    }
}