package com.techu.apitechudb.Controller;

import com.techu.apitechudb.Models.UserModel;
import com.techu.apitechudb.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")
public class UserController {
    @Autowired
    UserService userService;
    @GetMapping ("/users")
    public ResponseEntity<List<UserModel>> getUser(){
        System.out.println("getUser");

        return new ResponseEntity<>(
                this.userService.findAll(),
                HttpStatus.OK
        );
    }
    @GetMapping("/users1/{age}")
    public ResponseEntity<Object> getUserByAge(@PathVariable String age){
        System.out.println("getUserByAge");
        System.out.println("La edad del usuario es " + age);

        int age2 =Integer.parseInt(age);
        Optional<UserModel> result = this.userService.findByAge(age2);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserByid(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del usuario es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/users")
    public  ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del usuario a crear es " + user.getId());
        System.out.println("El nombre del usuario a crear es" + user.getName());
        System.out.println("La edad del usuario a crear es" + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );

    }
        @PutMapping("/users/{id}")
        public  ResponseEntity<UserModel> updateUser(@RequestBody UserModel user,@PathVariable String id){
            System.out.println("La id del usuario que se va a actualizar en parametro es " + id);
            System.out.println("La id del usuario que se va a actualizar es " + user.getId());
            System.out.println("El nombre del usuario que se va a actualizar es " + user.getName());
            System.out.println("La edad del usuario que se va a actualizar es " + user.getAge());

            return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);
        }

        @DeleteMapping("/users/{id}")
        public ResponseEntity<String> deleteUser(@PathVariable String id){
            System.out.println("deleteUser");
            System.out.println("La id es " + id);

            boolean deleteUser = this.userService.delete(id);
            return  new ResponseEntity<>(
                    deleteUser ? "Usuario borrado" : "Usuario no encontrado",
                    deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
            );
        }
    }

